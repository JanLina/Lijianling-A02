nodeJS + MongoDB

1、在根目录运行 npm install 下载所需模块

2、进入mongo目录，运行 node insert.js 初始化数据库

3、返回根目录，运行 node index.js --inputMode="file" --outputFormat="xml" ，其中 --inputMode 可为"file"或"database"，--outputFormat 可为"xml"或"json"