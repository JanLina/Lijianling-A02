// 导入模块
let fs = require('fs'),
	yargs = require('yargs'),	
	js2xmlparser = require('js2xmlparser'),
	mongoose = require('mongoose');

// 配置命令行参数
let argv = yargs.demand(['inputMode', 'outputFormat']) // 是否必选
				.describe({                            // 参数提示
					inputMode: '"file" or "database"', 
					outputFormat: '"xml" or "json"'
				})
				.argv;

// Prototype模式模拟类
function ZoneHandler(ipToDomainFile, zoneConfigFile, zoneXmlInfoFile, zoneJsonInfoFile) {
	this.ipToDomainFile = ipToDomainFile;
	this.zoneConfigFile = zoneConfigFile;
	this.zoneXmlInfoFile = zoneXmlInfoFile;
	this.zoneJsonInfoFile = zoneJsonInfoFile;
	this.ipToDomain = [];
}

// 读取ip和域名的映射文件，将映射关系保存到this.ipToDomain
// 并调用this.handleData方法进行数据的下一步处理
ZoneHandler.prototype.start = function() {
	let self = this;

	fs.readFile(this.ipToDomainFile, function(err, data) {
		if (err) {
			return console.error('读取文件ipToDomain.txt出错：' + err);
		}

		data = data.toString().split('\n');
		data.forEach(function(item, index, arr) {
			item = item.split('\t');
			self.ipToDomain.push({
				ip: item[0],
				domain: item[1],
				isReplace: item[2].replace('\r', '')
			});
		});

		self.handleData();
	});
}

// 根据命令行参数读入数据 & 输出数据
ZoneHandler.prototype.handleData = function() {
	if (argv.inputMode === 'file') { // 从文本文件读入数据
		console.log('从' + this.zoneConfigFile + '读入数据...');
		this.readFromFile();
	} else { // 从数据库读入数据
		console.log('从数据库读入数据...');
		this.readFromDB();
	}
}

ZoneHandler.prototype.readFromFile = function() {
	let zoneConfigFile = this.zoneConfigFile,
		zoneXmlInfoFile = this.zoneXmlInfoFile,
		zoneJsonInfoFile = this.zoneJsonInfoFile,
		self = this;
	fs.readFile(zoneConfigFile, function(err, data) {
		if (err) {
			return console.error('读取文件' + zoneConfigFile + '出错：' + err);
		}

		// 将从文本文件读取的字符串转换为对象，方便后面转换为xml或json
		data = self.toObjArray(data);

		if (argv.outputFormat === 'xml') { // 转换为xml格式
			console.log('以xml格式输出到' + zoneXmlInfoFile + '...');		
			let xml = self.toXmlOrJson('xml', data); 
			self.writeFile(xml, zoneXmlInfoFile); 
		} else {  // 转换为json格式
			console.log('以json格式输出到' + zoneJsonInfoFile + '...');
			let json = self.toXmlOrJson('json', data);
			self.writeFile(json, zoneJsonInfoFile);
		}
	});
}

ZoneHandler.prototype.readFromDB = function() {
    require('./mongo/index.js');
    
	let Zone = mongoose.model('Zone'),
		zoneXmlInfoFile = this.zoneXmlInfoFile,
		zoneJsonInfoFile = this.zoneJsonInfoFile,
		self = this;
	Zone.find({}, function(err, results) {
		if (err) {
			return console.error('从数据库读取数据出错：', err);
		}

		if (argv.outputFormat === 'xml') { // 转换为xml格式并写入文件
			console.log('以xml格式输出到' + zoneXmlInfoFile + '...');		
			let xml = self.toXmlOrJson('xml', results); 
			self.writeFile(xml, zoneXmlInfoFile); 
		} else {  // 转换为json格式并写入文件
			console.log('以json格式输出到 zoneJsonInfo.txt ...');
			let json = self.toXmlOrJson('json', results);
			self.writeFile(json, zoneJsonInfoFile);
		}
	});
}

ZoneHandler.prototype.toObjArray = function(data) {
	data = data.toString().split('\n');
	data.forEach(function(item, index, arr) {
		item = item.split('\t');
		arr[index] = {
			name: item[0],
			ip: item[1],
			port: item[2],
			currentNumOfPeople: item[3],
			maxNumOfPeople: item[4].replace('\r', '')
		};
	});
	return data;
}

// 将数据转换为xml或json
ZoneHandler.prototype.toXmlOrJson = function(format, data_arr) {
	let servers_arr = [],
		self = this,
		obj;
	obj = (format === 'xml') ? {
		"servers": {
			"server": []
		},
		"zones": {
			"zone": []
		}
	} : {
		"servers": [],
		"zones": []
	}

	data_arr.forEach(function(item, index, arr) {

		// 若servers数组中没有这个ip/port，将其添加进去
		let server = item.ip + '/' + item.port;
		if (servers_arr.indexOf(server) === -1) {
			servers_arr.push(server);
		}

		// 筛选掉"$"开头的zone
		if ((/^\$/).test(item.name)) {
			return;
		}

		// 填入obj
		if (format === 'xml') {
			obj.zones.zone.push({
				"@": {
					"name": item.name,
					"serverIndex": servers_arr.indexOf(server) + 1,
					"capacity": self.calcCapacity(item)
				}
			});
		} else {
			obj.zones.push({
				"name": item.name,
				"serverIndex": servers_arr.indexOf(server) + 1,
				"capacity": self.calcCapacity(item)
			});
		}
	});

	// 填入obj
	servers_arr.forEach(function(server, index, arr) {

		// 将符合条件的ip转换为域名
		self.ipToDomain.forEach(function(item, index, arr) {
			if (item.ip === server.split('/')[0] && item.isReplace == 1) {
				let port = server.split('/')[1];
				server = item.domain + '/' + port;
			}
		});

		if (format === 'xml') {
			obj.servers.server.push({
				"#": server
			});
		} else {
			obj.servers.push({
				"ip": server.split('/')[0],
				"port": server.split('/')[1]
			});
		}
	});

	// 将obj解析为xml字符串 / json字符串
	return (format === 'xml') ? js2xmlparser.parse("root", obj) : JSON.stringify(obj, null, 4);
}

ZoneHandler.prototype.writeFile = function(xmlString, fileName) {
	fs.writeFile(fileName, xmlString, function(err) {
		if (err) {
			return console.error(err);
		}
	});
}

ZoneHandler.prototype.calcCapacity = function(item) {

	// 表驱动，阶梯访问表
	let rangeLimit = [10, 30, 40, 50, 70, 80, 100],
		capacity = [1, 2, 3, 4, 5, 6, 7],
		maxCapacityLevel = capacity.length - 1,
		capacityLevel = 0,
		zoneCapacity = 7,
		p = item.currentNumOfPeople / item.maxNumOfPeople * 100;

	while (zoneCapacity === 7 && capacityLevel < maxCapacityLevel) {
		if (p < rangeLimit[capacityLevel]) {
			zoneCapacity = capacity[capacityLevel]
		}
		capacityLevel++;
	}

	return zoneCapacity;
}


// 新建 ZoneHandler 实例，传入输入文件和输出文件的路径
let zoneHandler = new ZoneHandler('input/ipToDomain.txt', 'input/zoneConfig.txt', 'output/zoneXmlInfo.txt', 'output/zoneJsonInfo.txt');
// 调用start方法开始进行数据的处理
zoneHandler.start(); 

