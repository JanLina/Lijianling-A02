let mongoose = require('mongoose'),
	uri = 'mongodb://localhost:27017/train';

mongoose.connect(uri);

let ZoneSchema = new mongoose.Schema({
	name: String,
	ip: String,
	port: Number,
	currentNumOfPeople: Number,
	maxNumOfPeople: Number
});

mongoose.model('Zone', ZoneSchema);
