var mongoose = require('mongoose');
require('./index.js');
var Zone = mongoose.model('Zone');


Zone.remove({}, function(err) {
	if (err) {
		return console.error('移除zones下所有文档出错：' + err);
	}

	// 通过model就可以增删改查mongodb中的文档
	let zones = [
		{
			name: '阿尔岛',
			ip: '192.168.1.5',
			port: 8000,
			currentNumOfPeople: 20,
			maxNumOfPeople: 300
		},
		{
			name: '可可岛',
			ip: '192.168.1.5',
			port: 8000,
			currentNumOfPeople: 60,
			maxNumOfPeople: 400
		},
		{
			name: '奥比岛',
			ip: '192.168.1.5',
			port: 8000,
			currentNumOfPeople: 90,
			maxNumOfPeople: 300
		},
		{
			name: '波比岛',
			ip: '192.168.1.6',
			port: 8000,
			currentNumOfPeople: 60,
			maxNumOfPeople: 300
		},
		{
			name: '奥拉岛',
			ip: '192.168.1.6',
			port: 8000,
			currentNumOfPeople: 70,
			maxNumOfPeople: 300
		},
		{
			name: '奥奇岛',
			ip: '95.63.1.100',
			port: 8003,
			currentNumOfPeople: 50,
			maxNumOfPeople: 300
		},
		{
			name: '奥雅岛',
			ip: '95.68.31.115',
			port: 8001,
			currentNumOfPeople: 180,
			maxNumOfPeople: 300
		},
		{
			name: '$test',
			ip: '95.68.31.115',
			port: 8003,
			currentNumOfPeople: 1,
			maxNumOfPeople: 300
		}
	];

	Zone.create(zones, function(err) {
		console.log('save status:', err ? 'failed' : 'success');
	});

});